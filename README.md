This is Tim Michaels' submission to the Quantaverse Software Engineering Test.

The file titled 'suspicious_transactions.csv' contains a list of transactions fulfilling the specified criteria.

The file titled 'suspicious_entities.csv' contains a list of senders and recievers ranked by number of suspicious transactions.

The script is contained in the notebook extract_suspicious_transactions.ipynb. The user can adjust two threshholds. One for how large a percentage an outgoing transaction must be of an incoming transaction to be considered suspicious. The default is 90%. The second parameter is to tune how small the dollar amounts must be to be considered. The default is $1,000,000.

Thanks for your consideration.

Best,
Tim